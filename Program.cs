﻿using System;
namespace TpExoCsharp;

class Program
{
    static void Main(string[] args)  
    {
        while (true) {
            Console.WriteLine("Bienvenue! Que voulez vous faire? ");
            Console.WriteLine("");
            Console.WriteLine("[1] Compte Banque");
            Console.WriteLine("[2] Cercle");
            Console.WriteLine("[3] Jeux De Dés");
            Console.WriteLine("[4] Horloge");
            Console.WriteLine("[5] Exit");

            string input = Console.ReadLine();

            if (input == "1")
            {
                CompteBanque.Compte();
            }
            else if (input == "2")
            {
                Cercle.Program.CercleCalculate();
            }
            else if (input == "3")
            {
                JeuxDeDes.Jouer();
            }
            else if (input == "4")
            {
                Horloge.HorlogeApp();
            }

            else if (input == "5")
            {
                Console.WriteLine("Au revoir!");
                Environment.Exit(0);
            }
        }
    }
}
