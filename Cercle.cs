using System;

namespace Cercle
{
    public class Point
    {
        private double x, y;

        public double Y
        {
            get { return y; }
            set { y = value; }
        }

        public double X
        {
            get { return x; }
            set { x = value; }
        }

        public Point(double x = 0, double y = 0)
        {
            this.x = x;
            this.y = y;
        }

        public void Display()
        {
            Console.WriteLine("POINT ({0},{1})", x, y);
        }

        public double Distance(Point origine)
        {
            double x0 = origine.x, y0 = origine.y;
            return Math.Sqrt(((x - x0) * (x - x0) + ((y - y0) * (y - y0))));
        }
    }

    public class Cercle
    {
        private Point centre;
        private double rayon;

        public double Rayon
        {
            get { return rayon; }
            set { rayon = value; }
        }

        public Point Centre
        {
            get { return centre; }
            set { centre = value; }
        }

        public Cercle(Point centre, double rayon = 0)
        {
            this.centre = centre;
            this.rayon = rayon;
        }

        public Cercle()
        {
            centre = new Point();
            centre.X = 0;
            centre.Y = 0;
            rayon = 0;
        }

        public double GetPerimeter()
        {
            return 2 * Math.PI * rayon;
        }

        public double GetSurface()
        {
            return Math.PI * rayon * rayon;
        }

        public bool IsInclude(Point pt)
        {
            return (pt.Distance(centre) <= rayon);
        }

        public void Display()
        {
            Console.WriteLine("CERCLE(({0}, {1}), {2})", centre.X, centre.Y, rayon);
        }
    }

    public class Program
    {
        public static void CercleCalculate()
        {
            Console.WriteLine("Donner l'abscisse du centre :");
            double x = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Donner l'ordonnée du centre :");
            double y = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Donner le rayon :");
            double rayon = Convert.ToDouble(Console.ReadLine());

            Point centre = new Point(x, y);
            Cercle cercle1 = new Cercle(centre, rayon);

            cercle1.Display();
            Console.WriteLine("Le périmètre est " + cercle1.GetPerimeter());
            Console.WriteLine("La surface est " + cercle1.GetSurface());

            Console.WriteLine("Donner les coordonnées d'un point :");
            double Xpoint = Convert.ToDouble(Console.ReadLine());
            double Ypoint = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("X: " + Xpoint);
            Console.WriteLine("Y: " + Ypoint);
            Point verify = new Point(Xpoint, Ypoint);

            Console.WriteLine(cercle1.IsInclude(verify));
        }
    }
}
